<?php

/*
 * Collect input
 */

$visitor = [
    'ip_address' => inet_pton($_SERVER['REMOTE_ADDR']),
    'user_agent' => $_SERVER['HTTP_USER_AGENT'], 
    'view_date' => date('Y-m-d H:i:s', $_SERVER['REQUEST_TIME']), 
    'page_url' => $_SERVER['HTTP_REFERER'], 
    'hashtag' => '',
];

$data_for_hash = implode('', [
    $_SERVER['HTTP_REFERER'], 
    $_SERVER['HTTP_USER_AGENT'], 
    $_SERVER['REMOTE_ADDR'],
]);

$visitor['hashtag'] = md5($data_for_hash);

/*
 * Save to DB
 */

$dsn = 'mysql:host=127.0.0.1;dbname=infuse_test';
$username = 'infuse';
$password = 'infuse';
$options = [ 
    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
];

try {
    $db = new PDO($dsn, $username, $password, $options);
    $stmt = $db->prepare(
        'insert into visitors (ip_address, user_agent, view_date, page_url, hashtag)
         values (:ip_address, :user_agent, :view_date, :page_url, :hashtag)
         on duplicate key update view_count = view_count + 1'
    );
    $stmt->execute($visitor);
} catch (Exception $e) {
    file_put_contents('logs/errors.log', $e->getMessage());
}

/*
 * Send image to page
 */

$file = new SplFileObject("assets/img/road-1920x1000.jpg", "rb");

header("Content-Type: image/jpg");
header("Content-Length: " . $file->getSize());

$file->fpassthru();

exit;

// $filename = 'assets/img/road-1920x1000.jpg'; 
// $handle = fopen($filename, "rb"); 

// if ($handle !== false) {
//     $contents = fread($handle, filesize($filename)); 
//     fclose($handle); 

//     header("content-type: image/jpeg"); 
//     echo $contents; 
// }
